PACT setup:
- PACT broker is constantly running for the CI/CD pipeline to interact with, in our case it is a docker container
=> we need setup instructions for the docker container.
- gradle plugin included in the project
- webhooks need to be defined in the PACT broker to feed back into the pipeline, regarding passing / failing tests

Consumer side (pact-client)
--------------
- Step 1. Run the PACT test via the IDE, created the PACT json file, (this file is not commited to git repo)
- Step 2. gradlew pactPublish => pushes json PACT to the broker.


Provider side (pact-demo-server)
-------------
- Step 1a. gradlew test (verify pacts in unit test)
- Or Step 1b. gradlew pactVerify (pact verification triggered by broker)

Deploying pact broker
-------------
https://hub.docker.com/r/pactfoundation/pact-broker/
