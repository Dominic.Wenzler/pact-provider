package com.pact.example.pact.demo;

import au.com.dius.pact.provider.junit5.HttpTestTarget;
import au.com.dius.pact.provider.junit5.PactVerificationContext;
import au.com.dius.pact.provider.junitsupport.Provider;
import au.com.dius.pact.provider.junitsupport.State;
import au.com.dius.pact.provider.junitsupport.loader.PactBroker;
import au.com.dius.pact.provider.junitsupport.loader.PactBrokerAuth;
import au.com.dius.pact.provider.junitsupport.loader.PactFolder;
import au.com.dius.pact.provider.junitsupport.target.Target;
import au.com.dius.pact.provider.junitsupport.target.TestTarget;
import au.com.dius.pact.provider.spring.junit5.PactVerificationSpringProvider;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;

import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.ComponentScan;

import org.springframework.test.context.junit.jupiter.SpringExtension;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Provider("test_provider_demo")
@ComponentScan(basePackages = "com.pact.example.pact.demo")
@ExtendWith(SpringExtension.class)
//@PactBroker(url = "${pactBrokerUrl}",
//		authentication =  @PactBrokerAuth(username = "${pactBrokerUser}", password = "${pactBrokerPassword}")
//)
@PactBroker(url = "http://PactS-Pactb-KJPPCP4C0LVP-1721369512.eu-west-1.elb.amazonaws.com:80",
		authentication =  @PactBrokerAuth(username = "", password = "")
)

//	@PactFolder("build/pacts")  // use this for loading pacts from local filesystem
class PactServerDemoApplicationTest {

	@LocalServerPort
	private int port;

	@TestTemplate
	@ExtendWith(PactVerificationSpringProvider.class)
	void pactVerificationTestTemplate(PactVerificationContext context){
		context.verifyInteraction();
	}

	@State("test state")
	public void provisionTestState(){

	}
	@BeforeEach
	void before(PactVerificationContext context) {
		context.setTarget(new HttpTestTarget("localhost", port));
	}



}
