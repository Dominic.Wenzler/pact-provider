package com.pact.example.pact.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PactServerDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PactServerDemoApplication.class, args);
	}

}
