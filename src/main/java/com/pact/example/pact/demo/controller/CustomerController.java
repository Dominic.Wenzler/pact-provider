package com.pact.example.pact.demo.controller;

import com.pact.example.pact.demo.domain.Customer;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerController {

    @GetMapping(
            value = "/customer",
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public Customer getCustomer(){
        Customer customer = new Customer();
        customer.setAge(345);
        customer.setName("Muster");
        customer.setSurename("Hans");
        return customer;
    }
}
